package Raumschiff;
import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;


/**
 * 
 * @author Gurjit Kaur
 * Raumschiffklasse
 * 23.03.2021
 * @version 1.0
 */

public class Raumschiff extends Ladung {

	/**
	 * parameterloser Konstruktor 
	 */
	
	private int anzahlTorpedoes;
	private int energieversorgungInProzent;
	private int huelleInProzent;
	private int schutzschildInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int anzahlAndroiden;
	private String raumschiffName;
	private static ArrayList<String> broadCastCommunicator = new ArrayList<String>();
	private ArrayList<String> logbucheintraege;
	private ArrayList<Ladung> ladungRaumschiff;
	
	/**
	 * 
	 * @param name - Name des Raumschiffes
	 * @param energieVersorgung - Prozent der Energieversorgung 
	 * @param statusLebenserhaltungsSystem - Prozent des Lebenserhaltungssystems
	 * @param statusSchutzschild - Prozent des Schutzschildes
	 * @param statusHuelle - Prozent der H�lle
	 * @param anzahlPhotonentorpedoes - Anzahl der Photonentorpedoes 
	 * @param anzahlAndroiden - Anzahl der Androiden 
	 */

	

	
	public Raumschiff(int anzahlTorpedoes, int energieversorgungInProzent, int huelleInProzent, int schildeInProzent,
			int lebenserhaltungssystemeInProzent, int anzahlAndroiden, String raumschiffName) {

		setAnzahlTorpedoes(anzahlTorpedoes);
		setEnergieversorgungInProzent (energieversorgungInProzent);
		setHuelleInProzent(huelleInProzent);
		setSchutzschildInProzent(schutzschildInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAnzahlAndroiden(anzahlAndroiden);
		setRaumschiffName(raumschiffName);
		this.logbucheintraege = new ArrayList<String>();
		this.ladungRaumschiff = new ArrayList<Ladung>();
	}

	
	public String getRaumschiffName() {
		return raumschiffName;
	}

	public void setRaumschiffName(String raumschiffName) {
		this.raumschiffName = raumschiffName;
	}
	
	public int getSchutzschildInProzent() {
		return schutzschildInProzent;
	}

	public void setSchutzschildInProzent(int schutzschildInProzent) {
		this.schutzschildInProzent = schutzschildInProzent;
	}
	
	public int getEnergieversorgungInProzent () {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAnzahlTorpedoes() {
		return anzahlTorpedoes;
	}

	public void setAnzahlTorpedoes(int anzahlTorpedoes) {
		this.anzahlTorpedoes = anzahlTorpedoes;
	}

	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}

	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}
	
	/**
	 * 
	 * @return ladungRaumschiff
	 */
	public ArrayList<Ladung> getLadungRaumschiff() {
		return ladungRaumschiff;
	}

	public void setLadungRaumschiff(ArrayList<Ladung> ladungRaumschiff) {
		this.ladungRaumschiff = ladungRaumschiff;
	}
	/**
	 * 
	 * @param sachen
	 */
	public void ladungHinzufuegen(Ladung sachen) {
		this.ladungRaumschiff.add(sachen);
	}
	/**
	 * 
	 * @param Schiff
	 */
	public void feuerTorpedoes(Raumschiff Schiff) {
		if (this.anzahlTorpedoes > 0) {
			System.out.println("Photonentorpedos abgeschossen");
			treffer(Schiff);
			this.anzahlTorpedoes = -1;
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	/**
	 * 
	 * @param Schiff 
	 */
	public void feuerPhaser(Raumschiff Schiff) {
		if (this.energieversorgungInProzent > 49) {
			System.out.println("Photonentorpedos abgeschossen");
			treffer(Schiff);
			this.energieversorgungInProzent = -50;
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	/**
	 * 
	 * @param Schiff - Anzahl der Treffer
	 */

	private void treffer(Raumschiff Schiff) {
		System.out.println("" + Schiff.raumschiffName + " wurde getroffen!");
		if (Schiff.schutzschildInProzent > 49) {
			Schiff.schutzschildInProzent = -50;
		} else {
			Schiff.huelleInProzent = -50;
			Schiff. energieversorgungInProzent = -50;
			if (Schiff.huelleInProzent < 0) {
				System.out.println("" + Schiff.raumschiffName + "s Lebenserhaltene Systeme wurden zerst�rt!");
				Schiff.lebenserhaltungssystemeInProzent = 0;
			}
		}

	}
	
	/**
	 * @param Nachricht - Inhalt der Nachricht senden
	 */
	public void nachrichtAnAlle(String Nachricht) {
		System.out.println("" + Nachricht + " wurde im broadcastKommunikator hinzugef�gt!");
		broadCastCommunicator.add(Nachricht);
	}
	
	
	/**
	 * Eintragungenen im folgenden Parametern
	 * @param Schutzschild
	 * @param Huelle
	 * @param Energie
	 * @param AndroidenAnzahl
	 */
	public ArrayList<String> Logbucheintrag() {
		return this.logbucheintraege;
	}
	
	/**
	 * @param energieversorgungInProzent - Soll aufgeladen werden?
	 * @param schutzschildInProzent - Soll repariert werden?
	 * @param huelleInProzent - Soll repariert werden?
	 * @param AndroidenAnzahl - Wie viele sollen rausgesendet werden?
	 */
	
	public void raumschiffReperatur(Boolean Schutzschild, Boolean Huelle, Boolean Energie, int AndroidenAnzahl) {
		if (AndroidenAnzahl <= this.anzahlAndroiden) {
			int zufall;
			int reperaturwert;
			zufall = (int) (Math.random() * 100);
			if (Schutzschild & Huelle & Energie) {

				reperaturwert = zufall * AndroidenAnzahl / 3;
				this.huelleInProzent = +reperaturwert;
				this.schutzschildInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Schutzschild & Huelle) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.schutzschildInProzent = +reperaturwert;

			} else if (Huelle & Energie) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Huelle & Energie) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Huelle) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.huelleInProzent = +reperaturwert;
			} else if (Energie) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Schutzschild) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.schutzschildInProzent = +reperaturwert;

			}
		} else {
			System.out.println(" Nicht genug Reperaturandroiden vorhanden!");
		}

	}
	
	

	/**
	 * @ param statusRaumschiff - Ausgabe Status des Raumschiff
	 */
	public void statusRaumschiff() {
		System.out.println("Raumschiffname:" + this.raumschiffName);
		System.out.println("Energie:" + this.energieversorgungInProzent + "%");
		System.out.println("H�lle:" + this.huelleInProzent + "%");
		System.out.println("Schutzschild:" + this.schutzschildInProzent + "%");
		System.out.println("Lebenserhaltene Systeme:" + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Torpedoes:" + this.anzahlTorpedoes);
		System.out.println("Androiden:" + this.anzahlAndroiden);
	}

	
	/**
	 * @param ladungEntleeren - Ladung leeren
	 */
	
	public void ladungEntleeren() { 

		for(int i = 0; i < this.ladungRaumschiff.size(); i++) { 

			if(this.ladungRaumschiff.get(i).getAnzahl() == 0) { 

				this.ladungRaumschiff.remove(i); 

				} 

		} 

	} 
	
	/**
	 * @param statusLadung - Abfragen der Ladung
	 */

	public void statusLadung() {
		System.out.println("" + this.raumschiffName + " enth�lt:");
		for (int i = 0; i < this.ladungRaumschiff.size(); i++) {
			System.out.printf("%s: %d\n", this.ladungRaumschiff.get(i).getBezeichnung(), this.ladungRaumschiff.get(i).getAnzahl());
		}
	}
	
}
