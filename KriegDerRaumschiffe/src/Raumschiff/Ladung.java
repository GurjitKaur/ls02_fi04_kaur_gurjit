package Raumschiff;

	/**
	 * 
	 * @author Gurjit Kaur
	 * Ladungsklasse
	 * 23.03.2021
	 *  @version 1.0
	 */


public class Ladung {

	private int anzahl;
	private String bezeichnung;
	
	/**
	 * Konstruktor f�r die Ladungsklasse mit den Parametern:
	 * @param art - Ladungsbezeichnung
	 * @param anzahl - Ladungsanzahl
	 */
	public Ladung() {

	}
	
	/**
	 * Ausgabe der Ladungsparametern als seperate Strings:
	 * @param art - Ladungsbezeichnung
	 * @param anzahl - Ladungsanzahl
	 */

 
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		setAnzahl(anzahl);
	}
	
	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int zaehlung) {
		this.anzahl = zaehlung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}

	
	/**
	 * kein parameter
	 */
	public void ladungEntleeren () {
		this.bezeichnung =null;
		this.anzahl=0; 
	}


}
