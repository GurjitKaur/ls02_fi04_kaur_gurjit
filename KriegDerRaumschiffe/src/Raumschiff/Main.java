 package Raumschiff;
 
 	/**
 	 * 
 	 * @author Gurjit Kaur
 	 *Main Methode
 	 *23.03.2021
 	 * @version 1.0
 	 */

public class Main {

	/**
	 * 
	 * @param args
	 */

       public static void main(String[]args) {

            

             Ladung saft= new Ladung("Ferengi Schneckensaft", 200);

             Ladung borgSchrott= new Ladung("Borg-Schrott", 5);

             Ladung roteMaterie= new Ladung("Rote Materie", 2);

             Ladung forschungssonde= new Ladung("Forschungssonde", 35);

             Ladung torpedoes= new Ladung("Photonentorpedo", 3);

             Ladung plasmaWaffe= new Ladung("Plasma-Waffe", 50);

             Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);

            

            

            

             Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");

             Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Kharzara");

             Raumschiff vulkanier = new Raumschiff(0, 80, 50, 80, 100, 5, "Ni'Var");

      
             
             klingonen.ladungHinzufuegen(saft);
             
             klingonen.ladungHinzufuegen(schwert);
             
             romulaner.ladungHinzufuegen(borgSchrott);

             romulaner.ladungHinzufuegen(roteMaterie);

             romulaner.ladungHinzufuegen(plasmaWaffe);
             
             vulkanier.ladungHinzufuegen(forschungssonde);

             vulkanier.ladungHinzufuegen(torpedoes);
             
             klingonen.feuerTorpedoes(romulaner);
             
             romulaner.feuerPhaser(klingonen);
              
             romulaner.nachrichtAnAlle("Gewalt ist nicht logisch");
             
             klingonen.statusRaumschiff();
             
             klingonen.statusLadung();
             
             vulkanier.raumschiffReperatur(true, true, true, 5);
             
             vulkanier.setAnzahlTorpedoes(torpedoes.getAnzahl());
                 
             vulkanier.ladungEntleeren();
               
             klingonen.feuerTorpedoes(romulaner);

             klingonen.statusRaumschiff();
             
             klingonen.statusLadung();
             
             romulaner.statusLadung();

             romulaner.statusRaumschiff();

             vulkanier.statusRaumschiff();

             vulkanier.statusLadung();
             
             klingonen.feuerTorpedoes(romulaner);
             
           
            
             
             
       }

}